<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pizza;
use Validator;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return App\Module::all();
        return Pizza::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        // ¡¡¡ FORMULARIOS NO en api !!!
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->validate($request, [
        //    'code' => 'required|unique:studies|max:6',
        //    'name' => 'required|max:255|min:2'
        //]);


        $rules = [
            'name' => 'required',
            'user_id' => 'required'
        ];



        Pizza::create($request->all());
        return ['created' => 'ok'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Pizza::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        //
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pizza = Pizza::findOrFail($id);
        $pizza->id = $request->id;
        $pizza->name = $request->name;
        $pizza->user_id = $request->user_id;

        $pizza->save();
        return redirect('/pizzas/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$module = Module::findOrFail($id);
        //$module->delete();

        Pizza::destroy($id);
        return ['deleted' => 'ok'];
    }
}
