<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    public function type()
    {
        return $this->belongsTo('App\Type');

        //muchos ingredientes pertenecen a un tipo (n:1)
    }

    public function user()
    {
        return $this->belongsTo('App\User');

        //muchos ingredientes pertenecen a un usuario (n:1)
    }

    public function pizzas()
    {
        return $this->belongsToMany('App\Pizza');

        //muchos ingredientes tienen muchos pizzas (n:m)
    }

}
