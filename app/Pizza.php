<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{

    public function user()
    {
        return $this->belongsTo('App\User');

        //muchas pizzas pertenecen a un usuario (n:1)
    }

    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient');

        //muchas pizzas tienen muchos ingredientes (n:m)
    }
}
