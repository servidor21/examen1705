<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function ingredients()
    {
        return $this->hasMany('App\Ingredient'); //un tipo tiene muchos ingredientes (1:n)
    }
}
