@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de estudios
</h1>

<div class="form">
<form action="/studies/{{ $study->id }}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}


    <div class="form-group">
        <label>Código: </label>
        <input type="text" name="code" value="{{ $study->code }}">
    </div>

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ $study->name }}">
    </div>

    <div class="form-group">
        <label>Nombre Corto: </label>
        <input type="text" name="shortName" value="{{ $study->shortName }}">
    </div>

    <div class="form-group">
        <label>Abreviatura: </label>
        <input type="text" name="abreviation" value="{{ $study->abreviation }}">
    </div>

    <div class="form-group">
        <label>Nivel del estudio: </label>

        <select type="select" name="level_id">
            @foreach ($levels as $level)
            <option value="{{ $level->id }}"
                @if ($level->id == old('level_id',  $study->level_id)) 
                    selected 
                @endif >
                {{ $level->name }}
            </option>
            @endforeach                
        </select>

    </div>
    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>    
</form>
</div>
</div>
@endsection
