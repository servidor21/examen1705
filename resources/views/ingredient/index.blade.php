@extends('layouts.app')

@section('content')
    <h1>Listado de ingredientes</h1>
<!--    @can('create', 'App\Pizza')
        <a href="/pizzas/create">Nuevo</a>
    @else
        No puedes dar altas!!
    @endcan
-->

    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Nombre tipo ingrediente</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($ingredients as $ingredient)
            <tr>
                <td>  {{ $ingredient->id }} </td>
                <td>  {{ $ingredient->name }} </td>
                <td>  {{ $ingredient->type->name }} </td>
                <td>  
                    <form method="post" action="/ingredients/{{ $ingredient->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $ingredient)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $ingredient)
                        <a href="/ingredients/{{ $ingredient->id }}/edit">Editar</a>
                        @endcan

                        @can('view', $ingredient)
                        <a href="/ingredients/{{ $ingredient->id }}"> Ver </a>
                        @endcan
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $ingredients->render() }}

@endsection('content')
