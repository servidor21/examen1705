@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de estudios
</h1>

<ul>
    <li>Codigo: {{ $study->code }}</li>
    <li>Abreviatura: {{ $study->abreviation }}</li>
    <li>Nombre: {{ $study->name }}</li>
    <li>Nombre Corto: {{ $study->shortName }}</li>
    <li>Nivel del estudio: {{ $study->level->name }}</li>
</ul>

<p><a href="/studies/{{ $study->id }}/edit">Modificar</a></p>

    <h3>Módulos del estudio actual</h3>
    <ol>
        @foreach ($study->modules as $module)
        <li>{{ $module->code }} - {{ $module->name }} - Curso  {{ $module->pivot->course }}º</li>
        @endforeach
    </ol>
</div>
@endsection
