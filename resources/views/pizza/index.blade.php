@extends('layouts.app')

@section('content')
    <h1>Listado de pizzas</h1>
<!--    @can('create', 'App\Pizza')
        <a href="/pizzas/create">Nuevo</a>
    @else
        No puedes dar altas!!
    @endcan
-->

    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Usuario alta</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($pizzas as $pizza)
            <tr>
                <td>  {{ $pizza->id }} </td>
                <td>  {{ $pizza->name }} </td>
                <td>  {{ $pizza->user->name }} </td>
                <td>  
                    <form action="/pizzas/{{ $pizza->id }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <input type="submit" value="borrar">
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $pizzas->render() }}

@endsection('content')
