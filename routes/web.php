<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/', function () {
    return "Este contenido es sólo para usuarios";
})->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('/types', 'TypeController');
Route::resource('/ingredients', 'IngredientController');
Route::resource('/users', 'UserController');
Route::resource('/pizzas', 'PizzaController');

